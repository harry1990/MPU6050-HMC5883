#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include "IIC.h"
#include "delay.h"
//初始化IIC
void IIC_Init(void)
{					     
	GPIO_InitTypeDef GPIO_InitStructure;
	//RCC->APB2ENR|=1<<4;//先使能外设IO PORTC时钟 
	RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOB, ENABLE );	
	   
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;   //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
 
	IIC_SCL=1;
	IIC_SDA=1;

}
//产生IIC起始信号
void IIC_Start(void)
{
	SDA_OUT();     //sda线输出
	IIC_SDA=1;	  	  
	IIC_SCL=1;
	delay_us(4);
 	IIC_SDA=0;//START:when CLK is high,DATA change form high to low 
	delay_us(4);
	IIC_SCL=0;//钳住I2C总线，准备发送或接收数据 
}	  
//产生IIC停止信号
void IIC_Stop(void)
{
	SDA_OUT();//sda线输出
	IIC_SCL=0;
	IIC_SDA=0;//STOP:when CLK is high DATA change form low to high
 	delay_us(4);
	IIC_SCL=1; 
	IIC_SDA=1;//发送I2C总线结束信号
	delay_us(4);							   	
}
//等待应答信号到来
//返回值：1，接收应答失败
//        0，接收应答成功
u8 IIC_Wait_Ack(void)
{
	u8 ucErrTime=0;
	SDA_IN();      //SDA设置为输入  
	IIC_SDA=1;delay_us(1);	   
	IIC_SCL=1;delay_us(1);	 
	while(READ_SDA)
	{
		ucErrTime++;
		if(ucErrTime>250)
		{
			IIC_Stop();
			return 1;
		}
	}
	IIC_SCL=0;//时钟输出0 	   
	return 0;  
} 
//产生ACK应答
void IIC_Ack(void)
{
	IIC_SCL=0;
	SDA_OUT();
	IIC_SDA=0;
	delay_us(2);
	IIC_SCL=1;
	delay_us(2);
	IIC_SCL=0;
}
//不产生ACK应答		    
void IIC_NAck(void)
{
	IIC_SCL=0;
	SDA_OUT();
	IIC_SDA=1;
	delay_us(2);
	IIC_SCL=1;
	delay_us(2);
	IIC_SCL=0;
}					 				     
//IIC发送一个字节
//返回从机有无应答
//1，有应答
//0，无应答			  
void IIC_Send_Byte(u8 txd)
{                        
    u8 t;   
	SDA_OUT(); 	    
    IIC_SCL=0;//拉低时钟开始数据传输
    for(t=0;t<8;t++)
    {              
        IIC_SDA=(txd&0x80)>>7;
        txd<<=1; 	  
		delay_us(2);   //对TEA5767这三个延时都是必须的
		IIC_SCL=1;
		delay_us(2); 
		IIC_SCL=0;	
		delay_us(2);
    }	 
} 	    
//读1个字节，ack=1时，发送ACK，ack=0，发送nACK   
u8 IIC_Read_Byte(unsigned char ack)
{
	unsigned char i,receive=0;
	SDA_IN();//SDA设置为输入
    for(i=0;i<8;i++ )
	{
        IIC_SCL=0; 
        delay_us(2);
		IIC_SCL=1;
        receive<<=1;
        if(READ_SDA)receive++;   
		delay_us(1); 
    }					 
    if (!ack)
        IIC_NAck();//发送nACK
    else
        IIC_Ack(); //发送ACK   
    return receive;
}

u8 IIC_WriteData(u8 dev_addr, u8 reg_addr, u8 *pdata, u8 count)			 //向指定地址中写入数据
{
	u8 i;
	IIC_Start(); 
	IIC_Send_Byte(dev_addr);		 									 //写从器件地址
	IIC_Wait_Ack();
	
	IIC_Send_Byte(reg_addr);											 //写数据地址
	IIC_Wait_Ack();
	for(i=0; i<count; i++)
	{
           
		IIC_Send_Byte(*pdata);											  //写数据
		IIC_Wait_Ack();
	
		pdata++;
	}	
        	
	IIC_Stop();
      
  delay_us(10000);
	return 0;

}
u8 IIC_ReadData(u8 dev_addr, u8 reg_addr)						 //从指定地址中读取数据
{
	u8 Temp;
 
	IIC_Start();
       
	IIC_Send_Byte(dev_addr);
	IIC_Wait_Ack();

	IIC_Send_Byte(reg_addr);
	IIC_Wait_Ack();
	IIC_Start();

	IIC_Send_Byte(dev_addr+1);
	IIC_Wait_Ack();

	Temp=IIC_Read_Byte(0);
	IIC_Stop();		 
	return Temp;	
}



unsigned char IIC_ReadMPU(u8 byAD, u8 byRA, u8 * pData, u8 byCount )
{
	 u8 i;

    IIC_Start();                   /*启动总线*/
    IIC_Send_Byte(byAD);                /*发送器件地址*/   
    IIC_Wait_Ack();
    
    IIC_Send_Byte(byRA);                /*发送器件子地址---数据寄存器*/
    IIC_Wait_Ack();
    IIC_Start();                   /*重新启动总线*/

    IIC_Send_Byte(byAD + 1);//address_read
    IIC_Wait_Ack();

    for(i = 0; i < (byCount-1); i++)
    {
        *pData=IIC_Read_Byte(1);                /*接收数据并发送就答位*/  
        pData++;
    }

    *pData=IIC_Read_Byte(0);       /*发送非应位*/
    IIC_Stop();                    /*结束总线*/ 
    return(1);	
}


unsigned char IIC_ReadCmpData(unsigned char byAD, unsigned char * pData, 
                                unsigned char byCount)
{
    u8 i;

    IIC_Start();                   /*启动总线*/
    IIC_Send_Byte(byAD);                /*发送器件地址*/   
    IIC_Wait_Ack();

    for(i = 0; i < (byCount-1); i++)
    {
        *pData=IIC_Read_Byte(0);                /*接收数据并发送就答位*/  
        pData++;
    }

    *pData=IIC_Read_Byte(0);        /*发送非应位*/
    IIC_Stop();                    /*结束总线*/ 
    return(1);
}

