#ifndef _SYSTEM_CONFIG_H
#define _SYSTEM_CONFIG_H

#include "stm32f10x.h"
#include "stm32f10x_conf.h"
#include <stdio.h>
#include "misc.h"
//#include "ins.h"
#include "stm32f10x_it.h"
#include "IIC.h"
#include "MPU6050.h" 
#include "hmc.h"
//#include <acc_comp.h>
#include <math.h>

#define LED1_ON GPIOB->BRR|=1<<8
#define LED2_ON GPIOB->BRR|=1<<9
#define LED3_ON GPIOE->BRR|=1<<0
#define LED4_ON GPIOE->BRR|=1<<1

#define LED1_OFF GPIOB->BSRR|=1<<8
#define LED2_OFF GPIOB->BSRR|=1<<9
#define LED3_OFF GPIOE->BSRR|=1<<0
#define LED4_OFF GPIOE->BSRR|=1<<1


#define PLL_MUL	9		// PLL9��Ƶ

void Driver_Init(void);
void RCC_Config(void);
void GPIO_Config(void);
void RCC_Configuration(void);
void USART1_Config(u32 baud);
void NVIC_Config(void);
uint8_t UART_Res(void );
void UART1_ReportMotion(int16_t ax,int16_t ay,int16_t az,int16_t gx,int16_t gy,int16_t gz,int16_t hx,int16_t hy,int16_t hz);
void UART1_ReportIMU(int16_t yaw,int16_t pitch,int16_t roll,int16_t alt,int16_t tempr,int16_t press,int16_t IMUpersec);
void Timer_Config(void);
void Delay_Us(u32 a);
void Delay_Ms(u16 a);

#endif

