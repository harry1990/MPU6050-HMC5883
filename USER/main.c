#include "led.h"
#include "delay.h"
#include "sys.h"
#include "usart.h"
#include "IIC.h"
#include "System_Config.h"

uint16_t cmd;
char d_data[4];
extern int calflag;

 int main(void)
 {
	int i=0;
  int initaccgg[3]={0};
  int accgg[3]={0};
  int  magicdataa[4]={0};
  float compss[3]={0};
  float agg[3]={0};
	char IIC_ID = 0;
	
	//初始化部分
	delay_init();	    	 //延时函数初始化
	NVIC_Configuration();// 设置中断优先级分组
	uart_init(9600);	 //串口初始化为9600
	LED_Init();		  	 //初始化与LED连接的硬件接口
	IIC_Init();
	
	printf("ARHS hardware config OK \r\n");
	MPU6050GyroInit();	
  MPU6050AccInit(); 
  InitCmp();
  MPU6050ReadAcc(initaccgg);  
	
  IIC_ID = MPU6050ReadID();
	printf("%d\r\n",IIC_ID);	
	
	while(1)
	{
	      for(i=0;i<16;i++)
      {
              
         MPU6050ReadAcc(accgg);
        
         agg[0]+=accgg[0];
         agg[1]+=accgg[1];
         agg[2]+=accgg[2];
      }
      agg[0]*=0.0625;
      agg[1]*=0.0625;
      agg[2]*=0.0625;
      ReadCmpOut(&magicdataa[0],&magicdataa[1], &magicdataa[2]);
      
      compss[0]=magicdataa[0]; 
      compss[1]=magicdataa[1];
      compss[2]=magicdataa[2];
      printf("%.3f %.3f %.3f %.3f %.3f %.3f\r\n",agg[0],agg[1],agg[2],compss[0],compss[1],compss[2]);
	}	 
}
 


